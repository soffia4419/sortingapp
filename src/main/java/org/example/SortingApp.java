package org.example;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Scanner;
public class SortingApp{
    public static int[] sort( InputStream inputStream, int length ){
        if(length<=0 || length>10){
            throw  new IllegalArgumentException();
        }
        Scanner s = new Scanner(inputStream);
        int[] arr = new int[length];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = s.nextInt();
        }

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if(arr[j]>arr[i]){
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }

        System.out.println(Arrays.toString(arr));
        return arr;
    }
}
