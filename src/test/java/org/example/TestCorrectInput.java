package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import static junit.framework.Assert.assertEquals;


@RunWith(Parameterized.class)
public class TestCorrectInput {

    private String input;
    private String expectedOutput;
    private int arrayLength;

    public TestCorrectInput(String input, String output, int arrayLength) {
        this.input = input;
        this.expectedOutput = output;
        this.arrayLength = arrayLength;
    }

    @Test
    public void testUserInput() {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(input.getBytes());
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();

        System.setIn(inputStream);
        System.setOut(new PrintStream(outContent));

        SortingApp.sort(inputStream, arrayLength);

        assertEquals(expectedOutput, outContent.toString().trim());
    }

    @Parameterized.Parameters
    public static Collection<Object[]> correctInput(){
        return Arrays.asList(new Object[][]{
                {"1 2 3 4", "[1, 2, 3, 4]", 4},
                {"1","[1]", 1},
                {"1 2 4 3 6 4 3 2 5 18", "[1, 2, 2, 3, 3, 4, 4, 5, 6, 18]", 10}
        });
    }

}
