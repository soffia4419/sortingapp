package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayInputStream;

import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
public class TestIncorrectInput {

    private String input;
    private int length;

    public TestIncorrectInput(String input, int length) {
        this.input = input;
        this.length = length;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUserInput() {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(input.getBytes());
        System.setIn(inputStream);

        SortingApp.sort(inputStream, length);

    }

    @Parameterized.Parameters
    public static Collection<Object[]> incorrectInput(){
        return Arrays.asList(new Object[][]{
                {"", 0},
                {"1 2 3 4 5 7 8 6 9 12 11", 11},
                {"", -3}
        });
    }
}
